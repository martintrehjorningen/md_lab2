package com.ntnu.mobile.mth.lab2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.martin.lab2.R;

import java.util.ArrayList;

/**
 * Created by martin on 18.03.2018.
 * Class based on content from following website
 * https://stackoverflow.com/questions/22955003/populate-listview-with-arraylist-item-on-button-click
 */

public class CustomAdapter extends ArrayAdapter<rssItem>{
    private ArrayList<rssItem> rssItemsList = new ArrayList<rssItem>();
    private Context context;
    private Activity mainActivity;
    private LayoutInflater inflater;


    public CustomAdapter(ArrayList<rssItem> rssItemsList, Activity mainActivity, Context context) {
        super(mainActivity, R.layout.test, rssItemsList);
        this.context = context;
        inflater = mainActivity.getWindow().getLayoutInflater();
        this.rssItemsList = rssItemsList;
    }

    public View getView(int pos, View view, ViewGroup parent) {
        View itemView = view;
        final RecyclerView.ViewHolder holder;

        if (itemView == null) {
            itemView = inflater.inflate(R.layout.test, parent, false);
        }

        final rssItem currentItem = rssItemsList.get(pos);
        TextView title = itemView.findViewById(R.id.titleText);
        title.setText(currentItem.getTitle());
        TextView link = itemView.findViewById(R.id.linkText);
        link.setText(currentItem.getLink());
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, A2_RssPage.class);
                i.putExtra("link", currentItem.getLink());
                context.startActivity(i);
            }
        });
        return itemView;
    }
}
