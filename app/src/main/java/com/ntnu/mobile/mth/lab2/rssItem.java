package com.ntnu.mobile.mth.lab2;

/**
 * Created by mar10 on 15.03.2018.
 */

public class rssItem {
    private String title;
    private String link;

    public rssItem() {
        this.title = "";
        this.link = "";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
