package com.ntnu.mobile.mth.lab2;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.martin.lab2.R;

import java.util.ArrayList;

public class A3_preferences extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    //Public statics for get/set av sharedpreferences
    public static final String PREFERENCES_NAME = "prefs";
    public static final String PREFERENCES_URL = "URL";
    public static final String PREFERENCES_NUMBER_OF_ITEMS = "NOI";
    public static final String PREFERENCES_FREQUENCY_OF_UPDATES = "FOU";
    //Defaults for verdiene over
    public static final String DEFAULT_URL = "http://www.nrk.no/toppsaker.rss";
    public static final int DEFAULT_NUMBER_OF_ITEMS = 10;
    public static final int DEFAULT_FREQUENCY_OF_UPDATES = 10;

    //private gui-elementser
    private Spinner A3_spinner1;    // Number of items in feed
    private Spinner A3_spinner2;    // Frequency of updates
    private EditText A3_editText1;  // URL

    //private variabler
    private SharedPreferences prefs;
    private String url;
    private int numberOfItems;
    private int frequencyOfUpdates;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);

        //Shared preferences
        prefs = getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
        url = prefs.getString(PREFERENCES_URL, DEFAULT_URL);
        //numberOfItems = prefs.getInt(PREFERENCES_NUMBER_OF_ITEMS, DEFAULT_NUMBER_OF_ITEMS);
        frequencyOfUpdates = prefs.getInt(PREFERENCES_FREQUENCY_OF_UPDATES, DEFAULT_FREQUENCY_OF_UPDATES);

        //Sett opp GUI-elementer som variabler
        A3_spinner1 = findViewById(R.id.A3_spinner1);
        A3_spinner2 = findViewById(R.id.A3_spinner2);
        A3_editText1 = findViewById(R.id.A3_editText1);

        //Fyller spinnere med alternativer
        initSpinner1();
        initSpinner2();

        //Laster inn valgte alternativer og URL fra preferences
        A3_spinner1.setOnItemSelectedListener(this);
        A3_spinner1.setSelection(loadSpinner1());
        A3_spinner2.setOnItemSelectedListener(this);
        A3_spinner2.setSelection(loadSpinner2());
        A3_editText1.setText(loadEditText1());

        //Setter opp listener på OnTextChanged
        A3_editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString(PREFERENCES_URL, s.toString());
                edit.apply();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
        SharedPreferences.Editor edit = prefs.edit();

        Spinner spinner = (Spinner) parent;
        if(spinner.getId() == R.id.A3_spinner1)
        {
            int val = DEFAULT_NUMBER_OF_ITEMS;
            switch(i) {
                case 0: val = 10;
                case 1: val = 20;
                case 2: val = 50;
                case 3: val = 100;
            }
            Log.d("onItemSelected", "val " +val);
            edit.putInt(PREFERENCES_NUMBER_OF_ITEMS, val);
            edit.apply();
        }
        else if(spinner.getId() == R.id.A3_spinner2)
        {
            int val = DEFAULT_FREQUENCY_OF_UPDATES;
            switch(i) {
                case 0: val = 10;
                case 1: val = 60;
                case 2: val = 1440;
            }
            Log.d("onItemSelected", "val " +val);
            edit.putInt(PREFERENCES_FREQUENCY_OF_UPDATES, val);
            edit.apply();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Spinner spinner = (Spinner) parent;
        if(spinner.getId() == R.id.A3_spinner1) {
            A3_spinner1.setSelection(0);
        }
        else if(spinner.getId() == R.id.A3_spinner1) {
            A3_spinner1.setSelection(0);
        }
    }

    private String loadEditText1() {
        return prefs.getString(PREFERENCES_URL, "");
    }

    private int loadSpinner1() {
        int val = prefs.getInt(PREFERENCES_NUMBER_OF_ITEMS, DEFAULT_NUMBER_OF_ITEMS);
        switch(val) {
            case 10: val = 0;
            case 20: val = 1;
            case 50: val = 2;
            case 100: val = 3;
        }
        Log.d("loadSpinner1", "val " +val);
        return val;
    }

    private int loadSpinner2() {
        int val = prefs.getInt(PREFERENCES_FREQUENCY_OF_UPDATES, DEFAULT_FREQUENCY_OF_UPDATES);
        switch(val) {
            case 10: val = 0;
            case 60: val = 1;
            case 1440: val = 2;
        }
        Log.d("loadSpinner2", "val " +val);
        return val;
    }

    private void initSpinner1() {
        //Alternativer som skal inn i spinneren
        ArrayList<Integer> data = new ArrayList<Integer>();
        data.add(10);
        data.add(20);
        data.add(50);
        data.add(100);

        //Setter opp L1 til å bruke en adapter som inneholder dataen over
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        A3_spinner1.setAdapter(adapter);
    }

    private void initSpinner2() {
        //Alternativer som skal inn i spinneren
        ArrayList<Integer> data = new ArrayList<>();
        data.add(10);
        data.add(60);
        data.add(1440);

        //Setter opp L1 til å bruke en adapter som inneholder dataen over
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        A3_spinner2.setAdapter(adapter);
    }
}
