package com.ntnu.mobile.mth.lab2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.martin.lab2.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static com.ntnu.mobile.mth.lab2.A3_preferences.DEFAULT_FREQUENCY_OF_UPDATES;
import static com.ntnu.mobile.mth.lab2.A3_preferences.DEFAULT_NUMBER_OF_ITEMS;
import static com.ntnu.mobile.mth.lab2.A3_preferences.DEFAULT_URL;
import static com.ntnu.mobile.mth.lab2.A3_preferences.PREFERENCES_FREQUENCY_OF_UPDATES;
import static com.ntnu.mobile.mth.lab2.A3_preferences.PREFERENCES_NAME;
import static com.ntnu.mobile.mth.lab2.A3_preferences.PREFERENCES_NUMBER_OF_ITEMS;
import static com.ntnu.mobile.mth.lab2.A3_preferences.PREFERENCES_URL;

public class A1_main_activity extends AppCompatActivity {

    //private gui-elementer
    private Button A1_button1;
    private ListView A1_listView1;

    //private variabler
    private SharedPreferences prefs;
    private String url;
    private int numberOfItems;
    private int frequencyOfUpdates;
    ArrayAdapter<rssItem> adapter;
    ArrayList<rssItem> rssArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);

        //Shared preferences
        prefs = getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
        url = prefs.getString(PREFERENCES_URL, DEFAULT_URL);
        Log.d("oncreate", "pref url " +prefs.getString(PREFERENCES_URL, DEFAULT_URL));
        numberOfItems = prefs.getInt(PREFERENCES_NUMBER_OF_ITEMS, DEFAULT_NUMBER_OF_ITEMS);
        Log.d("oncreate", "number of items " +prefs.getInt(PREFERENCES_NUMBER_OF_ITEMS, DEFAULT_NUMBER_OF_ITEMS));
        frequencyOfUpdates = prefs.getInt(PREFERENCES_FREQUENCY_OF_UPDATES, DEFAULT_FREQUENCY_OF_UPDATES);

        //Sett opp GUI-elementer som variabler
        A1_button1 = findViewById(R.id.A1_button1);
        A1_listView1 = findViewById(R.id.A1_listView1);


        //Button onclick
        A1_button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchA3();
            }
        });

        //Initialize rss-feed
        new RssFetchTask().execute();
    }

    private void launchA3() {
        System.out.print("hallo");
        Intent i = new Intent(this, A3_preferences.class);
        startActivity(i);
    }

    /**
     * Klasse laget basert på innhold i følgende nettsider
     * https://developer.android.com/reference/org/xmlpull/v1/XmlPullParser.html
     * https://www.androidauthority.com/simple-rss-reader-full-tutorial-733245/
     */

    private class RssFetchTask extends AsyncTask<Void, Void, Void> {
        ArrayList<rssItem> titleList = new ArrayList<rssItem>();

        @Override
        protected Void doInBackground(Void... aVoids) {
            Document doc = getDataAsDocument();
            if (doc != null) {
                processDocument(doc);
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            updateRssFeed();
        }

        private void processDocument(Document doc) {

            //Setup for å hente ut alle items i en nodeliste
            rssArrayList = new ArrayList<rssItem>();
            Element rootElement = doc.getDocumentElement();
            Node channelNode = rootElement.getChildNodes().item(1);
            NodeList allItems = channelNode.getChildNodes();

            for (int i = 0; i < allItems.getLength() && i < numberOfItems; i++) {
                rssItem newRssItem = new rssItem();
                Node workingItem = allItems.item(i);
                if (workingItem.getNodeName().equalsIgnoreCase("item")) {
                    NodeList childList = workingItem.getChildNodes();
                    for (int j = 0; j < childList.getLength(); j++) {
                        if (childList.item(j).getNodeName().equalsIgnoreCase("title")) {
                            newRssItem.setTitle(childList.item(j).getTextContent());
                        } else if (childList.item(j).getNodeName().equalsIgnoreCase("link")) {
                            newRssItem.setLink(childList.item(j).getTextContent());
                        }
                    }
                }
                rssArrayList.add(newRssItem);
            }
        }

        private Document getDataAsDocument() {
            if(Objects.equals(url, "")) {
                url = DEFAULT_URL;
            }
            try {
                URL rssUrl = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) rssUrl.openConnection();
                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStream in = connection.getInputStream();
                    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
                    return builder.parse(in);
                }
            } catch (IOException | ParserConfigurationException | SAXException e) {
                Log.e("getDataAsDocument", "exception: " + e.getMessage());
                Log.e("getDataAsDocument", "exception: " + e.toString());
            }
            return null;
        }
    }

    private void updateRssFeed() {
        adapter = new CustomAdapter(rssArrayList, A1_main_activity.this, this);
        A1_listView1.setAdapter(adapter);
    }

}
